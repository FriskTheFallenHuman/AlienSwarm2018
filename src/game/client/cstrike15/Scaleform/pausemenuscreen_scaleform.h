#pragma once

#include "scaleformui/scaleformui.h"
#include "igameevents.h"
#include "matchmaking/imatchframework.h"

#include "hud.h"

// i bet they stripped this out because it's SHIT :sunglasses:

class CPauseMenuScreenScaleform : public ScaleformFlashInterface, public CGameEventListener, public IMatchEventsSink
{
protected:
	static CPauseMenuScreenScaleform* m_pInstance;

public:
	CPauseMenuScreenScaleform();
	virtual ~CPauseMenuScreenScaleform();

	void FireGameEvent(IGameEvent *event);
	
	void PrepareUIForPauseMenu(bool show);
	void DestroyDialog();
	void Hide();
	void Show();
	void FlashReady();
	void BasePanelRunCommand(SCALEFORM_CALLBACK_ARGS_DECL);
	
	static void ShowMenu(bool show);
	static void LoadDialog();
	static bool IsActive() { return m_pInstance != NULL; }
	static bool IsVisible() { return (m_pInstance != NULL && m_pInstance->m_bVisible1); }

private:
	bool m_bVisible1, m_bLoading, m_bVisible3;
	int slot;
};